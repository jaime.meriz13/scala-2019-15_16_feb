\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Discrete Fracture Network Background}{14}{0}{2}
\beamer@sectionintoc {3}{Discrete Fracture Network Models}{19}{0}{3}
\beamer@sectionintoc {4}{The Forward and Inverse Problems}{23}{0}{4}
\beamer@sectionintoc {5}{Responses and Outcomes}{28}{0}{5}
\beamer@sectionintoc {6}{Numerical Examples}{29}{0}{6}
\beamer@sectionintoc {7}{Conclusions}{35}{0}{7}
\beamer@sectionintoc {8}{Future Work}{36}{0}{8}
\beamer@sectionintoc {9}{Thanks!}{37}{0}{9}
